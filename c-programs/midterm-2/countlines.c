#include <stdio.h>
#include <stdlib.h>

void count_lines(FILE *file)
{
    // - counts the lines in the file
    // - measures the length of each line
    // - determines the line number of the longest line

    char buf[64];
    int lineno = 1;
    int current_line_size = 0;
    int lineno_of_biggest_line = -1;
    int biggest_line_size_so_far = -1;

    while (1) {
        if (NULL == fgets(buf, sizeof(buf), file)) {
            if (feof(file)) break;
            perror("fgets");
            exit(1);
        }

        for (int i = 0; buf[i] != '\0'; i++) {
            if (buf[i] == '\n') {
                if (current_line_size > biggest_line_size_so_far) {
                    lineno_of_biggest_line = lineno;
                    biggest_line_size_so_far = current_line_size;
                }
                lineno++;
                current_line_size = 0;
            }
            else {
                current_line_size++;
            }
        }
    }

    printf("line count: %d\nbiggest line: %d, at %d chars in length\n",
            lineno-1, lineno_of_biggest_line, biggest_line_size_so_far);
}

int main(int argc, char *argv[])
{
    if (argc <= 1) return 2;
    FILE *file = fopen(argv[1], "rt");
    if (NULL == file) {
        perror("fopen");
        return 1;
    }
    count_lines(file);
    fclose(file);
    return 0;
}
