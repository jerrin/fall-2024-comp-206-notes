#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Adds up all the digits in the given string.
 * sum_digits("123hello123") gives 12
 */
int sum_digits(char const *str)
{
    int total = 0;
    // using pointer arithmetic to advance the pointer one char at a time
    for (; *str != '\0'; str++)
    {
        if ('0' <= *str && *str <= '9') // crucial to use '0' i.e. 48, since 0 itself is NUL byte
            total += *str - '0'; // convert digit char into its numerical value
            // A cast would not work: total += (int)(*str); will treat '0' as 48 !
    }
    return total;
}

/** Adds up all the whole numbers separated by spaces in the given string.
 * sum_numbers("12 12 12 3 6") gives 45
 */
int sum_numbers(char const *str)
{
    int total = 0;
    char *endptr;

    while (*str != '\0') {
        total += strtol(str, &endptr, 10);
        // now endptr points to the first char after the number that was parsed at the beginning of
        // str.
        str = endptr; 
        // ^ this way, the next iteration picks up where parsing left off
        // strtol ignores _leading_ whitespace, so it will automatically skip over the spaces at
        // the beginning of the next chunk.
        // On the other hand, if we reached end-of-string, then now str will point to a '\0' and
        // we'll exit the loop.
    }

    return total;
}

int main(int argc, char *argv[])
{
    if (argc != 3) return 2;

    if (0 == strcmp("digits", argv[1]))
        printf("sum of digits in `%s' is %d\n", argv[2], sum_digits(argv[2]));
    else if (0 == strcmp("numbers", argv[1]))
        printf("sum of numbers in `%s' is %d\n", argv[2], sum_numbers(argv[2]));
    else {
        fprintf(stderr, "usage: ./review [numbers|digits] STR\n");
        return 1;
    }

    return 0;
}
