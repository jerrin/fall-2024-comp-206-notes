#include <stdio.h>
#include <string.h>
// ^ This time using the standard library
// function `strcmp` to compare strings.
// We include string.h to access the string-related functions
// from the standard library.

char const * const USERNAMES[] = {
	"jake",
	"alice",
	"bob"
};

char const * const PASSWORDS[] = {
	"supersecure",
	"password",
	"abc123"
};

int main(int argc, char * argv[])
{
	if (argc != 2) {
		printf("Wrong usage.\n");
		return 1;
	}

	char *username = argv[1];

	int const len = sizeof(USERNAMES)/sizeof(USERNAMES[0]);

	// Declare the index variable _outside_ the loop so
	// that when we find the matching username, we can refer to
	// the same index in the PASSWORDS array.
	// Had we done `for (int i = 0; ...)` then the variable `i`
	// is scoped _to the loop_ and cannot be referred to outside.
	int i = 0;
	for (i = 0; i < len; i++) {
		// strcmp(s1, s2) returns:
		// - a negative value when s1 (alphabetically) comes before s2
		// - zero when the strings are equal
		// - a positive value when s2 comes after s2
		// Go read `man strcmp`.
		if (0 == strcmp(username, USERNAMES[i])) {
			break; // exit the loop
		}
	}

	// at this point, if i == len, then no matching username was found.
	if (i == len) {
		printf("login: error: no such user `%s'\n", username);
		// The %s format specifier is used for printing strings.
		return 1;
	}

	// Otherwise `i` gives the index of the matched username.
	// Proceed to read the password:
	printf("Password: "); // print a prompt

	char password[32]; // create an array to hold the password the user will type.
	scanf("%31s", password);
	// In contrast with the scanf("%d", &age) example we saw,
	// we don't need to use an & for the `password` variable.
	// That's because the `password` variable is an array, and arrays are
	// already pointers to their first element.
	//
	// Using `scanf("%s", ...)` without a maximum size like %31s
	// IS A HUGE SECURITY VULNERABILITY.
	//
	// That's because scanf will read as much as the user gives, writing
	// into the given array. The array's length, however _is fixed_, and
	// scanf will happily continue writing beyond the end of the array.
	// This is called a _buffer overrun._
	// https://en.wikipedia.org/wiki/Buffer_overflow
	// ('Buffer' is a essentially low-level term for 'array'.)
	//
	// The size of `password` is 32 but the size limit given to `scanf` is 31.
	// Why?
	// The `password` array needs to hold a NUL byte, value 0, to signal
	// the end of the string. The size given to scanf excludes that NUL
	// byte.

	if (0 == strcmp(password, PASSWORDS[i])) {
		printf("login: success\n");
		return 0;
	}
	else {
		printf("login: error: incorrect password.\n");
		return 1;
	}

	// FURTHER EXERCISES:
	// - improve `string_eq` from class to do a full comparison, mimicking
	//   the behaviour of the library function `strcmp`.
	// - implement `int string_len(char *s)` to compute the length of the
	//   given string by traversing it until a NUL byte is encountered.
	//   The returned size should not count the NUL byte itself.
	//
	// You can check your work by asking an AI
	// "show me an implementation of strcmp/strlen in C"
}
