#include <stdio.h>

// We run the program like this:
// $ ./login USER
//
// And it either prints:
// No such user USER
// Or:
// Password for USER: ....
// at which point we type in the password which it checks.

char const * const USERNAMES[] = {
	"jake",
	"alice",
	"bob"
};

// In the above arrays, I did not write an explicit length in the square
// brackets. Because I gave an explicit initializer to the array, the
// compiler infers the length from the count of items in the initializer.

/** Checks equality of two strings by traversing them
    both at the same time.
    By using the `const` qualifier on the parameters, we promise we won't
    modify the contents of the strings.
 */
int string_eq(char const *str1, char const *str2)
{
	int i = 0;
	while (str1[i] != 0 && str2[i] != 0) {
		if (str1[i] != str2[i])
			return 0;
		i++;
	}
        // EXERCISE: rewrite this while loop into a for-loop.
	return !(str1[i] != 0 || str2[i] != 0);
}

int main(int argc, char * argv[])
{
	/** HANDLING COMMAND-LINE ARGUMENTS */
	// `argc` is the _argument count_
	// `argv` is the _argument values_

	// suppose I do: ./login jake
	// Then:
	// argv[0] = "./login"
	// argv[1] = "jake"
	// argc = 2

	if (argc != 2) {
		printf("Wrong usage.\n");
		return 1;
	}

	// Similar to doing:
	// USERNAME="$1"
	// in a bash script
	char *username = argv[1];

	// Determine the length of the USERNAMES array.
	// Idea: take the size in bytes of the whole array and divide by the
	// size in bytes of one element.
	int const len = sizeof(USERNAMES)/sizeof(USERNAMES[0]);

	// Recall: `sizeof` is a _compiler_ function.
	//
 	// It runs when the program is compiled, not when it is run.
	// The compiler replaces `sizeof(USERNAMES)` with the size in bytes of
	// that variable.
	//
	// USERNAMES is an array (of length 3) holding values of type `char const * const`.
	// `const` doesn't affect the size, so what's the size of one `char *`?
	//
	// `char *` means "pointer to character(s)".
	// A pointer is nothing more than a variable that holds a _memory address._
	// For values made up of multiple bytes, a pointer to such a value holds the
	// address of the _first_ byte of that value.
	//
	// All memory addresses (on a given platform) have the same size:
	// mimi has a 64-bit processor, that number of bits is precisely the
	// size of the memory addresses, so `char *` takes up 8 bytes.
	//
	// The calculation made by the compiler is therefore:
	// (array length * item size) / (size of item 0)
	// (3 * 8) / 8 = 3

	for (int i = 0; i < len; i++) {
		if (string_eq(username, USERNAMES[i])) {
			printf("Login successful!\n");
			return 0;
		}
	}

	printf("Login failed.\n");
	return 1;
	// EXERCISES:
	// Add passwords!
	// - make an array PASSWORDS
	// - use scanf("%s", password) and then string_eq(password, ...)
}






