#include <stdlib.h>
#include <stdio.h>

// void *malloc(size_t n);
// void free(void *p);

int *read_numbers(int *n)
{
    printf("How many numbers? ");
    scanf("%d", n);
    // int nums[*n];
    int *nums = malloc(*n * sizeof(int));
    for (int i = 0; i < *n; i++) {
        scanf("%d", &nums[i]);
    }
    return nums;
}

int main()
{
    int n;
    int *numbers = read_numbers(&n);

    for (int i = 0; i < n; i++) {
        printf("%d. %d\n", i+1, numbers[i]);
    }

    free(numbers);

    return 0;
}
