#include <stdio.h>
#include <stdlib.h>

/** Reads data from `file` in a loop, printing it all out to stdout. */
void cat(FILE *file)
{
    char buf[1024];
    while (1) {
        // fgets signals failure, such as end-of-file without reading anything, by returning NULL
        if (NULL == fgets(buf, sizeof(buf)-1, file)) {
            // determine if the "error" was merely that we reached end-of-file
            if (feof(file)) return; // we're done in that case
            // otherwise, a _real_ error occurred, so we should print an error message
            perror("fgets");
            // look at `man perror`
            exit(1);
        }

        // otherwise, we read up to 1023 characters from the file into `buf`
        // so we can print those back out, to stdout:
        printf("%s", buf);
        // food for thought: why not `printf(buf);` ?
    }
}

int main(int argc, char *argv[])
{
	// open user-provided path, or stdin if none
	FILE *input_file;
	if (argc == 1) {
		input_file = stdin;
	}
	else {
		input_file = fopen(argv[1], "r");
		if (NULL == input_file) {
			// prints the real error message,
			// with the prefix "fopen" first
			perror("fopen");
			return 1;
		}
	}

	cat(input_file);
	return 0;
}

// FOLLOW-UP / challenge task:
// Adjust the above program to open _two_ files given by the user,
// one for input and one for output.
// Use the same setup as above:
// - read into a buffer using fgets
// - but use fprintf to print to the FILE* of the output file.
