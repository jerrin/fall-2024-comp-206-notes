#include <stdio.h>
#include <stdlib.h>

/** Reads data from `file` in a loop, printing it all out to stdout. */
void cat(FILE *file)
{
	// Your task:
	// - create a char array of size 1024
	// - in an infinite loop:
	//   - use `fgets` to read at most 1023 bytes
	//     from `file` into that array
	//   - use `printf` to print the string in that array to stdout
	// - when fgets fails, check `feof`, and break from the loop
	//   - if feof is _false_, then an error occurred
	//     so use perror to print that error then exit(1);
}

int main(int argc, char *argv[])
{
	// open user-provided path, or stdin if none
	FILE *input_file;
	if (argc == 1) {
		input_file = stdin;
	}
	else {
		input_file = fopen(argv[1], "r");
		if (NULL == input_file) {
			// prints the real error message,
			// with the prefix "fopen" first
			perror("fopen");
			return 1;
		}
	}

	cat(input_file);
	return 0;
}

// FOLLOW-UP / challenge task:
// Adjust the above program to open _two_ files given by the user,
// one for input and one for output.
// Use the same setup as above:
// - read into a buffer using fgets
// - but use fprintf to print to the FILE* of the output file.
