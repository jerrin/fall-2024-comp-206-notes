#include <stdio.h>
#include <stdlib.h>

// vim:
// :vsp ---- vertical split
// C-w h ---- move to left window
// C-w l ---- move to right window

int // <- output type
sum(int n) // sum takes an int as input and returns an int as output
{
	int total = 0;
	/* // the slash-star is a block comment
	while(n --> 0)
	{
		total += n+1;
	}
	*/

	for(int i = 0; i <= n; i++)
	{
		total += i;
	}

	return total;
}

/* FIBONACCI example */

// Formal, recursive definition of fibonacci:
// fib(0) = 0
// fib(1) = 1
// fib(n) = fib(n-1) + fib(n-2)

// You can implement that definition directly as a recursive algorithm,
// but it's SUUUUUUPER slow when n reaches even modest values.
// For those in COMP/ECSE250, you know that it's O(2^n) complexity.
// For those of you not in those courses, that means "exponentially
// slow."

// Instead, we can implement a much faster algorithm to calculate the
// nth fibonacci number by having our program keep track of the current
// fib number and the next fib number.

// Initially, the current fib number is fib(0) i.e. 0 and the next fib
// number will be fib(1) i.e. 1.
// The idea is to repeat the following process n times to get the nth
// fibonacci number:
// - The new 'current fib number' is simply whatever the next fib number
//   is
// - The new 'next fib number' is the sum of the current and next fib
//   numbers.

// The following table shows the result of running this process:
// curr  next
//    0     1
//    1     1    (0+1)
//    1     2    (1+1)
//    2     3    (1+2)
//    3     5	 (2+3)
//    5     8
//    8    13
//   13    21
//   21    34

int fib(int n)
{
	int curr = 0, next = 1, new_next;

	for(int i = 0; i < n; i++)
	{
		new_next = curr + next;
		curr = next;
		next = new_next;
 	}	

	return curr;
}

int main()
{
	int sum_of_first_100 = sum(100);
	printf("sum of first 100 integers is: %d\n", sum_of_first_100);

	printf("the 8th fibonacci is: %d\n", fib(8));
}

/** FOLLOW-UP EXERCISE:
    The values in the fibonacci sequence also grow exponentially.
    In other words, they get really big really fast.
    Write a function
    int find_biggest_fib() { ... }
    that finds the largest number in the fibonacci sequence that can
    properly be represented in an `int`

    CHALLENGE: do this without the use of any undefined behaviour.

    Note: exceeding the maximum representable value of a signed integer
    in C is undefined behaviour! However, most compilers simply "wrap
    around" to negative numbers as a consequence of the 2's complement
    representation technique that is used for negative numbers, so in
    practice this particular undefined behaviour isn't such a big deal.
 */   
